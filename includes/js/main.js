$(document).ready(function($) {


    //Change header on scrolling
    $(document).scroll(function () {
        var scroll = $(this).scrollTop();
        if (scroll > 10) {
            $('body').addClass('scrolled');
        } else {
            $('body').removeClass('scrolled');
        }
    });


    //Sliders
    $('.js-slider').each(function() {
        var slider = $(this);
        var slider_data = {
            nav: false,
            arrows: {
                prev: '<a class="unslider-arrow prev"></a>',
                next: '<a class="unslider-arrow next"></a>',
            },
            autoplay: true,
            delay: 5000,
            speed: 1000,
            infinite: true,
        };
        slider.unslider(slider_data);
    });


    //Hamburger menu
    var menu_access = true;
    var menu_active = false;
    $('.js-hamburger').click(function() {
        var hamburger = $(this);
        if (menu_access == true) {
            menu_access = false;
            if (menu_active == false) {
                hamburger.addClass('cross-pre');
                setTimeout(function() {
                    hamburger.addClass('cross');
                }, 250);
                $('body').addClass('menu-active');
                $('.js-menu').slideDown(function() {
                    menu_access = true;
                    menu_active = true;
                });
            } else {
                hamburger.removeClass('cross');
                setTimeout(function() {
                    hamburger.removeClass('cross-pre');
                }, 250);
                $('body').removeClass('menu-active');
                $('.js-menu').slideUp(function() {
                    menu_access = true;
                    menu_active = false;
                });
            }
        }
    });


    //  Responsive table wrapper around tables
    $('table.js-table-responsive, .js-table-responsive table').each(function() {
        var table = $(this);
        table.wrap('<div class="table-responsive"></div>');
        table.removeClass('js-table-responsive');
    });


    //  Date and time pickers
    $('.js-date').kendoDatePicker({
        format: "dd-MM-yyyy",
    });
    $('.js-time').kendoTimePicker({
        format: "HH:mm",
        interval: 60,
    });
    $('.js-datetime').kendoDateTimePicker({
        format: "dd-MM-yyyy HH:mm",
    });


    //  File input handler
    $('.js-file').change(function(e) {
        var filename = '&nbsp';
        if ($(this).val().length) {
            filename = $(this).val();
        }
        $(this).parents('.file').find('.filename').html(filename);
    });










    //Trigger target elements to slide up/down
    $('[data-target-trigger]').click(function(e) {
        var element = $(this);
        var target = element.attr('data-target-trigger');
        var target_element = $('[data-target="'+target+'"]');
        if (target_element.length) {
            target_element.slideToggle();
        }
    });


    //Load images dynamically to reduce load time
    $('img[data-img-src]').each(function() {
        var element = $(this);
        var source = element.attr('data-image-src');
        element.attr('src', source);
    });


    //Validate regex in input elements
    $('[data-validate-regex]').each(function() {

    });




});


