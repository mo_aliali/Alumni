//Google Maps
var maps;
function google_maps() {

    //Only run code if a map element is found
    var map_element = document.getElementById('map');
    if (map_element != null) {

        //Define google maps coordinates
        var location = new google.maps.LatLng(52.373112, 4.887356);
        var center = new google.maps.LatLng(52.373112, 4.887356);
        if ($(document).width() <= 991) {
            center = location;
        }

        //Settings for the map
        var mapOptions = {
            zoom: 12,
            center: center,
            scrollwheel: false,
        }

        //Create the map element
        maps = new google.maps.Map(map_element, mapOptions);

        //Add marker to the map
        var marker = new google.maps.Marker({
            position: location,
            map: maps,
        });
        marker.setMap(maps);
    }
}



//-----------------------
//  Google Maps URL's
//-----------------------
//  1. Normal   (URL to google maps with coordinates)
//  https://www.google.com/maps/@{INSERT_MAP_LAT_COORDINATES},{INSERT_MAP_LONG_COORDINATES},{INSERT_MAP_ZOOM}z
//
//  2. Search   (URL to google maps with coordinates and marker through searching)
//  http://maps.google.com/maps?&z={INSERT_MAP_ZOOM}&mrt={INSERT_TYPE_OF_SEARCH}&t={INSERT_MAP_TYPE}&q={INSERT_MAP_LAT_COORDINATES}+{INSERT_MAP_LONG_COORDINATES}
//-----------------------


//-----------------------
//  Explaination URL 2. (http://maps.google.com/maps?&q=
//-----------------------
//  q   = LATITUDE + LONGITUDE
//  q   = {52.373112+4.887356}    (Example values)
//  q   = {Amsterdam+Nederland}
//
//  z   = ZOOM LEVEL
//  z   = {16}
//
//  mrt = TYPE SEARCH
//  mrt = {yp}
//
//  t   = TYPE MAP
//  t   = {m}
//
//  hl  = LANGUAGE
//  hl  = {nl, en, tr}
//-----------------------


//-----------------------
//  Reference URL 2. (http://maps.google.com/maps?&q=
//-----------------------
//  TYPE MAP (t)
//  m = Normal map
//  k = Satellite
//  h = Hybrid
//  p = Terrain
//
//  TYPE SEARCH (mrt)
//  q   = Search query  - (Searchable values: LAT & LONG, Country, City, Postalcode, Street, Housenumber)
//  all = All results
//  loc = Locations based search
//  yp  = Business search
//  kmlkml      = Community search
//  realestate  = Real estate search
//  websearch   = Searches webpages with a geographical position
//
//  OUTPUT FORMAT (output)
//  html    = Standard HTML
//  js      = Returns object literals and function calls
//  kml     = XML file
//  nl      = XML formatted NetworkLink
//
//  dragdir = JSON object containing reversed geocode and an encoded polyline for a given route
//  embed   = Embed into website (Deprecated)
//-----------------------