// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Add a class to indicate if javascript is enabled or not
$(document).ready(function() {
    $('body').removeClass('no-js').addClass('js');
});

//Image orientation
var image_data_missing = 0;
function image_orientations() {
    image_data_missing = 0;

    $('img').each(function() {
        var image = $(this);
        var img_width = image.get(0).naturalWidth;
        var img_height = image.get(0).naturalHeight;
        if (img_width >= img_height) {
            var img_class = 'img-horizontal';
        } else {
            var img_class = 'img-vertical';
        }

        //Update the class and resolution values
        if ((!image.attr('data-resolution')) || (image.attr('data-resolution') == '0x0')) {
            image.attr('data-resolution', img_width+'x'+img_height);
            image.removeClass('img-horizontal').removeClass('img-vertical').addClass(img_class);
        }

        //Keep track of images that weren't ready yet
        if (img_width == 0 || img_height == 0) {
            image_data_missing++;
        }
    });

    //Run the function again later because some image data wasn't complete yet
    if (image_data_missing > 0) {
        setTimeout(function() {
            image_orientations();
        }, 50);
    }
}
$(document).ready(function($) {
    image_orientations();
});